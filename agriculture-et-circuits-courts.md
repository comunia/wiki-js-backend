<!-- TITLE: Agriculture et circuits courts -->


# Les enjeux globaux
* Comment nourrir sainement une population mondiale en plein phase de croissance?
* Comment nourrir nos cheptels localement?
* Comment fournir du carbone renouvelable pour la production d’énergie, de biomatériaux et l’industrie chimique?
* Comment répondre à l’ensemble de ces enjeux en protégeant la biodiversité et en réduisant l’impact de ces activités sur le climat? 

[video](https://www.youtube.com/watch?v=gjtIl5B1zXI){.youtube} {.align-center}


# Les enjeux locaux
* Comment limiter la progression des espaces artificialisés afin de préserver les surfaces agricoles?
* Comment réduire les émissions de gaz à effet de serre produites par les exploitations agricoles?
* Comment limiter puis inverser l’érosion du maillage bocager?
* Comment éviter l’érosion des sols et préserver leur qualité?
* Comment favoriser le développement des filières agroalimentaires et augmenter la valeur ajoutée produite sur le territoire?

# Retours d'expériences

# Les acteurs du territoire
## Acteurs Institutionnels et services techniques
* [L'Agence de l'Eau Sein-Normandie](http://www.eau-seine-normandie.fr/)
* [La Chambre d'Agriculture de l'Orne](https://normandie.chambres-agriculture.fr/)
* [Le Conseil Regional de Normandie](https://www.normandie.fr/agriculture-0) 

## Associations et syndicats

* [InPACT Basse Normandie](http://inpactbn.blogspot.com/)
	* *[L'AFOCG61](http://www.interafocg.org/afocg61)*
	* *[L'ARDEAR](http://www.agriculturepaysanne.org/ardear-normandie)*
	* *[Le FRCIVAM](http://civambassenormandie.org/)*
 	* *[Le GRAB](http://bio-normandie.org/)*
* [Le Reseau AMAP](http://www.reseau-amap.org/amap-61.htm)
* [Terre de Liens](http://www.terredeliensnormandie.org/)
* [Les Jeunes Agriculteurs](https://fr-fr.facebook.com/pages/category/Agricultural-Cooperative/Jeunes-Agriculteurs-de-lOrne-611749315618129/)

## Sociétés privées

Transformation 
* [Fromagerie Gillot](http://fromageriegillot.fr/)

Services
* [Bois Bocage Energie](https://www.normandie.fr/agriculture-0) 
# Les pistes d'action
Un travail significatif a été réalisé par l’association Solagro afin de tracer une feuille de route collective pour répondre à ces grands enjeux à l’échelle du territoire français. Ce travail est résumé dans [le scénario Afterres 2050](agriculture-et-circuits-courts/scenario-afterres-2050) et s’articule autour des 6 grands champs d’action suivants: 

* Mieux nourrir la population en repensant la composition de notre assiette alimentaire
* Repenser les systèmes de cultures sur la base des principes d’agroécologie
* Développer un système d’élevage conciliant performance alimentaire, lutte contre les émissions de GES et bien être animal
* Préserver les surfaces agricoles en luttant contre l’artificialisation des terres
* Développer de nouveaux systèmes agro-pastoraux
* Évaluer les impacts des activités agricoles


# A voir
## Pages connexes
* [Programme Régional de Développement Agricole et Rural (PRDAR)](agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural)
* [Le scénario Afterres 2050](agriculture-et-circuits-courts/scenario-afterres-2050)
* [Panorama de l'agriculture et de l'agroalimentaire](agriculture-et-circuits-courts/panorama-de-lagriculture-et-de-lagroalimentaire)

## Bibliographie
* [Le Référenciel des Territoires Basse-Normandie - L'agriculture](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwjEmfbm-7zeAhXwyIUKHTISCK8QFjAAegQIBBAC&url=http%3A%2F%2Fwww.etudes-normandie.fr%2Fupload%2Fcrbn_cat%2F1%2F908_2944_1_5_L_Agriculture.pdf&usg=AOvVaw2B7nflftZbaYZeZwSyiB2E), Rapport de la DREAL, Avril 2014




