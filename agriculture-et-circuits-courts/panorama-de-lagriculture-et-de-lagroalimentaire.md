<!-- TITLE: Panorama de l'agriculture et de l'agroalimentaire de Normandie-->
<!-- SUBTITLE: Source : Chambre régionale d'agriculture Normandie -->

[Panorama de l'agriculture et de l'agroalimentaire de Normandie](https://normandie.chambres-agriculture.fr/lagriculture-normande/agriscopie-chiffres-cles/panorama-de-lagriculture-et-de-lagroalimentaire/)

**Contexte et Environnement**
[Quelques indicateurs](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/indicateurs.pdf)
[Chiffres clefs](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/chiffres_cle.pdf)
[Population agricole](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/population.pdf)
[Exploitations agricoles](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/exploitation.pdf)
[Terre](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/terre.pdf)
[Territoires, paysages et environnement](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/environnement.pdf)

**Diversité des productions animales**
[Lait](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/lait.pdf)
[Viande bovine](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/viande.pdf)
[Cheval](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/cheval.pdf) 
[Porc](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/porc.pdf)
[Volailles, œufs et lapins](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/volaille.pdf)
[Mouton](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/mouton.pdf)
[Produits de la mer](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/mer.pdf)

**Diversité des productions végétales**
[Herbe et surfaces fourragères](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/herbe.pdf)
[Grandes cultures](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/cultures.pdf)
[Lin](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/lin.pdf)
[Filière cidricole](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/cidre.pdf)
[Légumes](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/legume.pdf)
[Horticulture ornementale](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/horticulture.pdf)
[Forêt et filière bois](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/foret.pdf)

**Autres Filièrers**
[Valorisations non alimentaires](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/nomalim.pdf)
[Autres petites filières agricoles](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/petites_filieres.pdf)
[Agriculture biologique](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/agriculture-biologique-normandie.pdf)
[Produits sous signe de qualité](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/qualite.pdf)
[Industries agroalimentaires](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/iaa.pdf)
[Tourisme rural et circuits de proximité](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/506_Fichiers-communs/PDF/AGRISCOPIE/tourisme.pdf)
