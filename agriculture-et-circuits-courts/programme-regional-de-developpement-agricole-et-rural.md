<!-- TITLE: Programme Régional de Développement Agricole et Rural (PRDAR) -->
<!-- SUBTITLE: Source: Chambre régionale d'agriculture en Normandie -->

# Résumé
Le Ministère de l’Agriculture, de l’Agroalimentaire et de la Forêt, grâce aux fonds CASDAR (Compte d’Affectation Spéciale «Développement Agricole et Rural»), finance un Programme National de Développement Agricole et Rural (PNDAR) dont le **Programme Régional (PRDAR),** constitue la déclinaison en région.
 
**3 orientations stratégiques**
➡Augmenter l’autonomie et améliorer la compétitivité des agriculteurs et des exploitations françaises via des systèmes triplement performants.
➡Promouvoir la diversité des modèles agricoles et des systèmes de production.
➡Améliorer les capacités d’anticipation et de pilotage stratégique des agriculteurs et des acteurs des territoires.

**4 thématiques prioritaires**
➡Anticipation et adaptation aux dynamiques globales de changement.
➡Conception et conduite de systèmes de production diversifiés et économiquement viables dans tous les territoires.
➡Qualité et valorisation des produits.
➡Renouvellement des générations et des formes d’exercice de l’activité agricole.

**Un programme régional** : Un contrat d’objectifs conclu entre l’APCA et le Ministère constitue le cadre de référence à partir duquel les chambres d’agriculture de Normandie ont élaboré et conduisent les actions du PRDAR 2014-2020. Il définit leurs engagements :

* Une priorité donnée à l’innovation.
* L’accompagnement de la triple performance.
* La diffusion des connaissances.
* Le renforcement des partenariats avec la recherche, les instituts techniques, les autres organismes de développement, les établissements de formation.

👉 **Le PRDAR normand 2014-2020 : Un programme décliné en 10 actions**

# Actions
[Action 1: Hommes et métiers (AE01)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-1-hommes-et-metiers)
[Action 2: Energie et Valorisation Non Alimentaire (AE02)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-2-energie-et-valorisation-non-alimentaire)
[Action 3: Agriculture et élevage de précision (AE03)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-3-agriculture-et-elevage-de-precision)
[Action 4: Agroalimentaire et proximité (AE04)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-4-agroalimentaire-et-proximite)
[Action 5: Stratégie et pilotage d’entreprise (AE05)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-5-strategie-et-pilotage-dentreprise)
[Action 6: Vers plus d’agro-écologie (AE06)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-6-vers-plus-dagro-ecologie)
[Action 7: Dynamique agricole sur les territoires à enjeux (AE07)](http://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-7-dynamique-agricole-sur-les-territoires-a-enjeux)
[Action 8: Performance en agriculture biologique (AE08)](https://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-8-performance-en-agriculture-biologique)
[Action 9: Remplacement (AE09)](https://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-9-remplacement)
[Action 10: Gouvernance du programme (AE10)](https://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-10-gouvernance-du-programme)
[Action 11: Produit Pilote Régional (AE11)](https://comunia.lta.io/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural#action-11-produit-pilote-regional)

# Action 1: Hommes et métiers
[Action 1: Hommes et métiers (AE01)](https://normandie.chambres-agriculture.fr/prdar-ae01/)

*  Intégrer la dimension Travail comme facteur de compétitivité et de durabilité des exploitations.
*  Accompagner les agriculteurs managers à la conduite d’entreprise et à la gestion des ressources humaines (GRH).
*  Promouvoir les bonnes pratiques et les organisations du travail innovantes.
*  Développer la boîte à outils des conseillers sur le Travail et la GRH.
*  Promouvoir les métiers et recruter les futures générations d’agriculteurs.


**Les résultats remarquables :**
[La ferme virtuelle normande pour promouvoir les métiers de l’agriculture](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae01-ferme-virtuelle.pdf)
[Journées Les pieds dans les bottes](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae01-trucs-astuces.pdf) 
[Concours Trucs et astuces d’éleveurs en Normandie](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae01-trucs-astuces.pdf) 
[Théâtre Forum : petits Changements et grands projets](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae01-theatre.pdf)
[Série d’articles : Ça coince dans le GAEC](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAr_AE01_ca-coince-dans-le-GAEC.pdf)
[Calendrier règlementaire](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE01_calendrier-reglementaire.pdf)
   
# Action 2: Energie et Valorisation Non Alimentaire
[Action 2: Energie et Valorisation Non Alimentaire (AE02)](https://normandie.chambres-agriculture.fr/prdar-ae02/)

<img src="/uploads/ae-02.png " style="float: right;">

* Améliorer la performance énergétique des systèmes de production.
* Développer la méthanisation agricole.
* Créer une offre durable de biomasse cellulosique.
* Faire émerger de nouvelles valorisations : produits bio-sourcés,énergie.


**Les résultats remarquables** :
[Journées des méthaniseurs de Normandie](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae02-methanisation.pdf)
[Méthanisation : Logistique transport et épandage](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE02_methanisation.pdf)
[Communication COP21 Energie – Climat : l’agriculture est une solution !](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE02_communication-COP21.pdf)
# Action 3: Agriculture et élevage de précision
[Action 3: Agriculture et élevage de précision (AE03)](https://normandie.chambres-agriculture.fr/prdar-ae03/)

<img src="/uploads/ae-03.png " style="float: right;">

* Surveiller le marché de l’innovation,
* Évaluer les innovations les plus marquantes,
* Expérimenter de nouveaux modèles ou technologies en ferme,
* Produire des outils de conseil et d’aide à la décision

**Les résultats remarquables**
[Plateformes numériques expérimentales LoRa en agriculture](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae03-lora.pdf)
[Agri'Up](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae03-Agriup.pdf)
[Fiches automates d’alimentation](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae03-Automates.pdf)
[Intérêts de la modulation intraparcellaire de l’azote et du régulateur](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE03_modulation-azotee.pdf)
# Action 4: Agroalimentaire et proximité
[Action 4: Agroalimentaire et proximité (AE04)](https://normandie.chambres-agriculture.fr/prdar-ae04/)

<img src="/uploads/ae-04.png " style="float: right;">

* Développer le nombre d’agriculteurs commercialisant des produits ou des services en circuits-courts et développer l’offre de produits.
* Structurer et professionnaliser l’offre en produits et services de proximité.
* Sensibiliser les agriculteurs et leurs organisations à l’importance de la communication.

**Les résultats remarquables** :
[Les RDV Pro Alim](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae04-pro-alim.pdf)
[Partenariats entre lycées agricoles et hôteliers](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae04-partenariat-lycee.pdf)
[Création d’un point de vente partenaire Bienvenue à la ferme](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE04_point-de-vente-partenaire-BALF.pdf) 
[Guide des marchés publics en restaurants collectifs en Basse-Normandie](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE04_guide-marches-publics.pdf)

# Action 5: Stratégie et pilotage d’entreprise
[Action 5: Stratégie et pilotage d’entreprise (AE05)](https://normandie.chambres-agriculture.fr/prdar-ae05/)

<img src="/uploads/ae-05.png " style="float: right;">

* Initier et organiser une synergie entre 3 catégories d’acteurs : le terrain, le conseil et le back office pour conseiller au plus juste les chefs d’exploitation.
* Organiser la diffusion des connaissances entre les différents acteurs qui alimentent le conseil.
* Accompagner l’acquisition de compétences professionnelles du conseil

**Les résultats remarquables**
[Boîte à outils du conseiller](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae05-bao-conseiller.pdf)
[Outil pour piloter le coût de production de l’atelier laitier](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae05-cout-de-production.pdf)
[Barcamp](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae05-Barcamp.pdf)
[Trésorerie : L’outil Arc-en-Ciel, du suivi vers le prévisionnel](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae05-outil-arc-en-ciel.pdf)
[Capacité d’adaptation et robustesse des exploitations bovines](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE05_robustesse.pdf)
[Nouvelle offre de formation pour le parcours à l’installation aidée (DJA)](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE05_parcours-installation.pdf)
[Observatoire Agri’scopie en ligne](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE05_agriscopie.pdf)

# Action 6: Vers plus d’agro-écologie
[Action 6: Vers plus d’agro-écologie (AE06)](https://normandie.chambres-agriculture.fr/prdar-ae06/)

<img src="/uploads/ae-06.png " style="float: right;">

* Caractériser l’agro-écologie en Normandie et mesurer les évolutions potentielles des principaux systèmes normands.
* Capitaliser les références et expériences d’agriculteurs s’inscrivant pleinement dans l’agro-écologie et les faire connaître au plus grand nombre.
* Expérimenter de nouvelles techniques, nouvelles cultures, nouveaux systèmes.
* Accompagner le changement auprès des agriculteurs. 

**Les résultats remarquables**
[ORACLE Normandie, un observatoire du climat](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae06-ORACLE-Normandie.pdf)
[Vidéos Réduction des intrants](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae06-videos-reduction-intrants.pdf)
[Le repérage des innovations, Vers une organisation normande](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE06_traque-innovation.pdf)
[Le diagnostic triple performance](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE06_Diag3P.pdf) 

# Action 7: Dynamique agricole sur les territoires à enjeux
[Action 7: Dynamique agricole sur les territoires à enjeux (AE07)](https://normandie.chambres-agriculture.fr/prdar-ae07/)

<img src="/uploads/ae-07.png " style="float: right;">

* Sensibiliser les acteurs présents sur les territoires pour créer les conditions propices à l’émergence de projets multi-acteurs.
* Accompagner la concrétisation de projets agricoles multi-acteurs et de filières territorialisées à multiples enjeux.
* Évaluer et promouvoir les projets aboutis dans une logique de diffusion et de développement. 

**Les résultats remarquables**
[Une réalisation automatisée de fiches « portrait agricole de territoire »](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae07-territoire-automatise.pdf)
[Une réalisation labellisée de « fiches expériences », vecteur de communication auprès des collectivités locales](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae07-fiches-experiences.pdf)
[Diagnostics biodiversité, de l’exploitation au territoire](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE07_diagnostic-biodiversite.pdf)
# Action 8: Performance en agriculture biologique
[Action 8: Performance en agriculture biologique (AE08)](https://normandie.chambres-agriculture.fr/prdar-ae08/)

<img src="/uploads/ae-08.png " style="float: right;">

*  Les chambres d’agriculture actrices du développement de la bio en région.
*  Faire découvrir des systèmes de production et des pratiques agricoles alternatives transposables à un public d’agriculteurs conventionnels et de prescripteurs.
*  Développer la boîte à outils bio des chambres pour le conseil et l’accompagnement.
*  Accompagner les projets innovants en installation et conversion. 

**Les résultats remarquables**
[Publication : Réussir ses cultures bio en Normandie](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae08-reussir-cultures-bio.pdf)
[Publication : Vivre des légumes biologiques en Normandie](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae08-castypes-legumes-ab.pdf)
[Guide de la Conversion](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae08-guide-conversion-AB.pdf) 
[Fiches témoignage sur la simplification du travail en élevage laitier bio](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae08-temoignage-BL-AB.pdf)
[4 ans de résultats d’essais sur l’autonomie alimentaire en AB](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE08_essai-autonomie-alimentaire.pdf)
[Bulletin d’information ACTU’BIO](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE08_ActuBio.pdf)
# Action 9: Remplacement
[Action 9: Remplacement](https://normandie.chambres-agriculture.fr/prdar-ae09/)

<img src="/uploads/ae-09.png " style="float: right;">

* Finaliser l’organisation du réseau et dynamiser l’échelon régional.
* Répondre aux besoins de remplacement au regard des évolutions économiques, techniques, sociales de la profession agricole.
* Sensibiliser les futurs exploitants et salariés à l’intérêt du remplacement.
* Simplifier les tâches administratives des services de remplacement.

**Les résultats remarquables**	
[Livret d’accueil Agent de remplacement](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae09-CQP-agent-remplacement.pdf)
[Organiser une formation CQP agent de remplacement pour former et qualifier des demandeurs d’emploi](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae09-CQP-agent-remplacement.pdf)
[Installation et remplacement : le pari gagnant !](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/PRDAR_AE09_installation-remplacement.pdf)
# Action 10: Gouvernance du programme
[Action 10: Gouvernance du programme](https://normandie.chambres-agriculture.fr/prdar-ae10/)

<img src="/uploads/ae-10.png " style="float: right;">

* Garantir le caractère innovant et l’efficience du PRDAR et de ses actions.
* Anticiper en assurant une surveillance de l’environnement.
* Piloter le programme en cohérence avec les autres programmes et les politiques publiques.
* Evaluer le programme de façon permanente et ponctuelle.
* Capitaliser et communiquer.
* Adapter et améliorer le programme de façon continue.

[Evaluation à mi-parcours de l'action AE03](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae10-evaluation-miparcours.pdf)
# Action 11: Produit Pilote Régional
[Action 11: Produit Pilote Régional](https://normandie.chambres-agriculture.fr/prdar-ae11/)

<img src="/uploads/ae-11.png " style="float: right;">

En cohérence avec les 10 actions du PRDAR, le PPR est conduit par la Chambre régionale d'agriculture de Normandie sous l'autorité de la DRAAF. Ce projet multi-partenarial vise à diffuser l'agro-écologie en Normandie et amplifier les dynamiques de groupes d'agriculteurs qui s'engagent dans le développement de projets agro-écologiques.

Un projet axé sur :
* la mobilisation des acteurs
* le partage et la diffusion des connaissances
* l'appropriation des innovations
* le développement des compétences
* l'exploration du pilier social
* la communication

17 partenaires engagés à la signature en novembre 2015. Possibilité de rejoindre le PPR à tout moment pour tous les partenaires qui le désirent.

**Les résultats remarquables**
[Prairiales 2017](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae11-prairiales-2017.pdf)
[Des Organisations de Producteurs (OP) « en marche » : Vers des projets communs entre acteurs des filières](https://normandie.chambres-agriculture.fr/fileadmin/user_upload/Normandie/012_Inst-Normandie/PDF/prdar-ae11-OP-en-marche.pdf)
		
