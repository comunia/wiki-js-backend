<!-- TITLE: Scénario Afterres 2050 -->


# Résumé
**Afterres2050 est le nom donné à un scénario d’utilisation des terres agricoles et forestières pour satisfaire les besoins alimentaires, énergétiques, en matériaux, et réduire les gaz à effet de serre.**

**Ce scénario est développé par l'ONG Solagro, pour le territoire français. Il complète et enrichit le [scénario négaWatt](/energie/scenario-nega-watt-2017), dont il partage les objectifs de « sobriété, efficacité et durabilité ».** Il sert de base aux hypothèses négaWatt sur le bois-énergie et le biogaz. Il a été construit avec le soutien de la Fondation Charles Léopold Mayer. Le scénario s'est limité à la France pour des raisons d'accès aux données, mais pourrait être étendu à d'autres pays, ou à l'Europe. De façon similaire, l'association négaWatt s'intéresse maintenant au paysage énergétique européen. Mis à part l' Association négaWatt Suisse, elle ne dispose pas d'associations européennes « amies », avec lesquelles elle pourrait échanger, et ne peut s'appuyer que sur les statistiques des administrations des pays européens étudiés. 

[video](https://www.youtube.com/embed/X84z2wcUbAc){.youtube} {.align-center}

# Objectifs
**Ce travail repose sur l'hypothèse simple qu'en 2050, l'agriculture et la forêt françaises pourront et devront (dans une approche de développement soutenable) nourrir en France environ 71 millions d’habitants.** Ces deux secteurs devront aussi nourrir les cheptels de bétail et volailles, tout en produisant assez de matériaux renouvelables et d'énergie pour répondre aux besoins de la société, en préservant la fertilité des sols, la qualité des eaux, la biodiversité, et sans continuer à affecter négativement le système climatique1. Le scénario intègre aussi le principe du facteur 4 (division par 4 des émissions françaises de gaz à effet de serre avant 2050).

Le scénario a pour objet de :

* **Savoir si assez de surfaces agricoles et rurales sont et seront disponibles** pour satisfaire à l'ensemble de ces besoins et contraintes, et à quelles conditions. Il a aussi cherché à comparer les visions de l'agriculture de demain ;
* **Baliser les voies possibles d'évolution vers une agriculture viable**, désirable et durable répondant à un scénario agricole et alimentaire durable, crédible, compréhensible et quantifié pour 2010-2050 ;
* **Produire des bases concrètes de travail**, et un cadre cohérent avec la partie énergie de la biomasse du scénario négaWatt 2011;
* **Offrir les éléments d'un débat citoyen et transversal sur les territoires**, en milieu agricole, pour l'orientation future de l'agriculture, en vue d'une interpellation des instances politiques et administratives impliquées dans « la mise en place effective des conditions nécessaires aux évolutions ».

C'est un outil qui peut notamment, comme le scénario négaWatt, aider les régions à affiner l'interface entre le SRCAE et le SRCE issus de la Loi Grenelle II. 

![Agro Ecologie](/uploads/agro-ecologie.jpg "Agro Ecologie"){.align-center}

# Moyens
Sur la base d'un travail collaboratif de construction de bases quantitatives et qualitatives des besoins actuel et futurs (pour déterminer les besoins en termes de nouvelle offre), **Afterres2050 a mobilisé les statistiques et éléments prospectifs disponibles, via un modèle simplifié d’utilisation du foncier agricole et en particulier des terres agricoles (modèle dit « MoSUT »)**. Ce modèle intègre des données physiques chiffrées (hectares, rendements, quantité d'aliments, régimes alimentaires) pour les hommes et les cheptels), qui est récursif (ajustement progressif et itératif des variables de la demande et de l'offre) et ascendant (agrégation des données maîtrisées par remontées successives).

* **Un outil complémentaire dit [« Climagri »](https://solagro.org/travaux-et-productions/outils/climagri) a été utilisé** pour simuler les impacts des scénarios possibles en termes d'émission de gaz à effet de serre.
* Au lieu de reposer sur une vision d'augmentation de l'offre (augmentation infinie ou indéfinie de la production), les scénarios intègrent, selon les données disponibles, l'impact probable du climat sur les rendements des cultures en France, l'impact des changements d'affectation de sols (Cf. périurbanisation et artificialisation par les réseaux de transports) ainsi que des notions de santé (« facteur (maîtrisé) d'augmentation de la masse corporelle de la population »).

# Apports
**Les modèles utilisés concluent qu'il est possible de répondre aux besoins alimentaires et énergétiques de tous les Français, en respectant le Facteur 4, par une agriculture à 50 % convertie à l'Agriculture biologique**. à condition de manger plus de céréales, de fruits et légumes, et beaucoup moins de viande, sucre et lait, de ne jamais laisser de sols nus et veiller à ce que chaque parcelle délivre jusqu'à six « productions » –céréales, engrais verts, fruitiers, bois d'œuvre– contre une le plus souvent en 2011. Le nombre de têtes de bétail devrait être fortement réduit et le pâturage plus extensif, pour permettre une alimentation plus à l'herbe et plus locale. La mise en œuvre de ce scénario libérerait plusieurs millions d'hectares (sur un total d'environ 55 millions d’hectares cultivés en 2010). Jusqu’à 20 % de la SAU (surface agricole utile) serait libérée par l'application du scénario Afterres2050 ; soit de 5 à 8 millions d'hectares (1/3 de terres arables, 2/3 de prairies permanentes). Ces surfaces pourraient être réaffectées et utilisables pour d'autres besoins : par exemple, la production de biomasse-énergie, de biomasse à destination de la chimie verte ou destinée à produire des matériaux renouvelables de construction ou d'isolation, etc.

# Conditions de réussite
Le scénario pose des conditions impliquant d'importants changements, qui impliquent des choix sociétaux (à la fois du politique et du citoyen, auquel les agriculteurs et toute la filière agroalimentaire doivent contribuer) ; ces conditions sont :

* **Une sobriété et une efficacité énergétique** « du champ à l'assiette » ;
* **Une alimentation moins lactée, et surtout moitié moins carnée**, car la production d'une unité de protéine animale nécessite une consommation bien supérieure de protéines végétales (4 à 10 fois plus selon le type d'animaux);
* **Des animaux moins nombreux**, mais élevés dans de meilleures conditions. Les troupeaux d'ovins pourraient être conservés à l'identique voire augmentés car ils peuvent valoriser des pâturages extensifs en milieux peu valorisables pour d'autres espèces.
* **Un élevage intensif est conservé comme sources de protéines animales « bon marché »**, en recherchant un équilibre entre demande sociale et environnement pour que la viande reste accessible aux catégories sociales moins riches, mais avec des ressources locales (suppression avant 2050 des importations de soja et tourteaux américains) ;
* **Le développement rapide de l'agroforesterie** qui devrait couvrir 1/5e des surfaces agricoles en 2050, plutôt que le soutien de l'afforestation sur les herbages ;
* **Des rotations plus importantes et des productions multiples** (« Une parcelle = 6 produits ») avec en 2050 une généralisation des associations culturales permettant de produire sur une même parcelle (et à la même saison) deux cultures (une céréale et une légumineuse en général), avec moins d'intrants et plus de biodiversité, pour une meilleure résilience agro-écologique. Ceci implique une démarche de production intégrée et de lutte biologique, avec des rotations longues, des assolements comprenant des légumineuses (engrais verts), une agriculture sans labour (travail très simplifié du sol et semis direct quand c'est possible) pour restaurer les capacités de puits de carbone du sol et restaurer une fertilité plus naturelle. Des cultures intercalaires doivent assurer un couvert permanent du sol (objectif zéro-érosion), avec restauration à large échelle des infrastructures agro-écologiques que sont les haies (de type bocagères), bandes enherbées, ripisylves, arbres épars, mares, fossés enherbés et autres zones humides, connectées à la Trame verte et bleue nationale pour bénéficier au mieux des auxiliaires de l'agriculture. Selon les modèles utilisés, ceci permettrait de diviser par 4 les usages d'engrais et de pesticides (ce qui est aussi une économie financière pour l'agriculteur), avec 50 % de la SAU en culture intégrée et sans traitements du tout sur un tiers des surfaces en agriculture biologique en 2050.
* **Une réduction des déséquilibres commerciaux internationaux** (exportations raisonnables et importations réduites d'intrants)
* **Le développement de « cultures énergétiques soutenables »** ; ce scénario propose que peu à peu, d'ici 2050, 1,5 million d'hectares de prairies servent à produire de la biomasse à méthaniser (« prairies énergétiques »). 1,5 million de terres labourables serviraient à produire d'une part des agrocarburants (à partir de céréales et oléoprotéagineux) et d'autre part, des tourteaux et protéines destinées à l'alimentation animale d'autre part. L'autoconsommation par l'agriculture de méthane issu des lisiers et fumiers rendrait le secteur plus indépendant, tout en réduisant considérablement les émissions de gaz à effet de serre de ce même secteur, et tout en conservant le potentiel humique des résidus de culture ou d'élevage méthanisés (car méthaniser ne détruit pas le carbone stable).

# Limites
Ce scénario a utilisé les bases qui sont celles des économistes et prospectivistes, ce qui le rend crédible.

Les limites qu'il expose lui-même sont dans les bouleversements qu'il nécessite ; les choix et actions qu'il suppose doivent être rapides et profonds ; ils concernent les stratégies, pratiques et moyens de l'agriculture. Ils concernent aussi l'aménagement du territoire, avec une ouverture sur l'agrosylviculture (déjà en cours dans 11 départements).

Les émissions de gaz à effet de serre en 2010-2011 étaient d'environ 105 MteqCO2 (dont 40 MteqCO2 de N2O, 40 MteqCO2 de méthane, et 25 MteqCO2 de gaz carbonique). En tenant compte des intrants agricoles (engrais, énergie, phytosanitaires, …), elles peuvent être réduites, mais les différentes simulations Afterres2050 arrivent au mieux à une réduction d'un facteur 21. Les auteurs estiment que le facteur 4 ne serait possible en agriculture qu'avec des ruptures difficilement possibles pour les secteurs concernés. 

# Sources
* [Page wikipédia du Scénario Afterres2050](https://fr.wikipedia.org/wiki/Afterres2050)

# Voir aussi
## Articles connexes
* [Le Scénario Negawatts](/energie/scenario-nega-watt-2017)

## Liens externes
* [Description du Scénario sur le site de Solagro](https://afterres2050.solagro.org/a-propos/le-projet-afterres-2050/)
* [La dernière version du Scénario Afterres 2050](http://afterres2050.solagro.org/wp-content/uploads/2015/11/Solagro_afterres2050-v2-web.pdf)
* [Un résumé de 4 pages du Scénario](http://afterres2050.solagro.org/wp-content/uploads/2015/11/De%CC%81pliant-Afterres2050-Web.pdf)