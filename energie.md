<!-- TITLE: Energie -->

# Les enjeux globaux
* Comment limiter le réchauffement moyen des températures mondiales?
* Comment limiter les tensions géopolitiques liées à l’approvisionnement énergétique?
* Comment produire de façon durable de l’énergie propre et accessible au plus grand nombre?

# Les enjeux locaux
* Comment réduire la consommation d’énergie primaire et d’énergie grise à l’échelle du territoire ?
* Comment améliorer la capacité de la population à répondre localement à ses besoins énergétiques ? 

# Les acteurs du territoires
-
# Les pistes d'action
A l’image du [scénario Afterres 2050](agriculture-et-circuits-courts/scenario-afterres-2050), l’association negaWatt a réalisé un travail présentant les actions nécessaires pour développer un mix énergétique neutre en carbone d’ici 2050. Le [scénario negaWatt](energie/scenario-nega-watt-2017) repose sur une dizaine de principes conducteurs, tels que:

* Une division par deux de la consommation d’énergie
* Une augmentation progressive de la part des énergies renouvelables dans le mix énergétique
* Une diminution progressive de l’utilisation de pétrole, de gaz fossile et de charbon
* Une mutation des pratiques agricoles et sylvicoles
* Une augmentation de l’autonomie énergétique du territoire

# A voir
## Pages connexes
*  [Le scénario negaWatt : Réussir la transition énergétique en France](energie/scenario-nega-watt-2017)
## Bibliographie

* [Le Référenciel des Territoires Basse-Normandie - L'énergie](http://www.normandie.developpement-durable.gouv.fr/IMG/pdf/1-6_L_Energie_RT_DREALBN_cle54a379.pdf), Rapport DREAL 2014