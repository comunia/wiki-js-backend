<!-- TITLE: Scenario negaWatt 2017 : Réussir la transition énergétique en France -->
<!-- SUBTITLE: Source : site negawatt.org -->

# Le scénario négaWatt
**Le scénario négaWatt** est un exercice prospectif pour la France : le futur qu’il explore ne constitue en rien une prédiction mais représente un chemin possible.  Il trace la voie d’un avenir  énergétique souhaitable et soutenable, et décrit des solutions pour l’atteindre.  Il a notamment pour but d’inciter et  d’aider les décideurs à intégrer les impératifs de long terme dans les décisions de court terme.

Cinq ans après le précédent exercice, le scénario négaWatt 2017-2050 est venu **confirmer la possibilité technique d’une France utilisant 100 % d’énergies renouvelables en 2050.**

[video](https://www.youtube.com/embed/jXIaQLCVB2M){.youtube} {.align-center}


➡ Site: https://negawatt.org/Scenario-negaWatt-2017-2050
# Contextes et enjeux
Entre  changement  climatique,  tensions  liées  aux  ressources  énergétiques  et  aggravation des risques sanitaires et technologiques, le poids des choix du passé en matière d’énergie est de moins en moins supportable. 
Pour  réussir  la  transition  énergétique,  l’action  doit  de  toute  urgence  être  renforcée.  C’est  dans  cette  perspective  que s’inscrit la publication du **scénario négaWatt 2017-2050** , qui vise à diminuer significativement l’ensemble des impacts environnementaux et des risques technologiques associés à notre système énergétique.

**Des engagements internationaux et nationaux**
Au niveau international (Accord de Paris) : contenir le réchauffement climatique nettement en-dessous de + 2°C.
En France :
● Diviser par quatre les émissions de gaz à effet de serre et par deux la consommation d’énergie finale d’ici 2050
● Réduire la part du nucléaire et augmenter celle des énergies renouvelables

![Negawatt](/uploads/negawatt.png "Negawatt"){.align-center}

# Principes fondamentaux
Le scénario négaWatt 2017-2050 applique à l’ensemble du système énergétique la « démarche négaWatt » consistant à :
* **Prioriser les besoins essentiels** dans les usages individuels et collectifs de l’énergie par des actions de sobriété (éteindre les  vitrines  et  les  bureaux  inoccupés la nuit, contenir 
l’étalement urbain, réduire les emballages, etc.) ;
* **Diminuer la quantité d’énergie** nécessaire à la satisfaction d’un  même  besoin  grâce  à  l’efficacité  énergétique (isoler les bâtiments, améliorer le rendement des appareils électriques ou des véhicules, etc.) ;
* **Privilégier  les  énergies renouvelables** pour leur faible impact  sur  l’environnement  et  leur  caractère inépuisable (ce sont des énergies de flux par opposition aux énergies de stock, fondées sur des réserves finies de charbon, pétrole, gaz fossile et uranium)

Le scénario négaWatt n’est pas un scénario de science-fiction, il ne nécessite aucune rupture technologique, économique ou sociétale: des évolutions profondes sont envisagées, mais pas de manière brutale. 

Il étudie  en  détail  les  différents  secteurs  de  consommation  (**bâtiment,  transport,  industrie, agriculture**) et de production d’énergie **(renouvelables, fossiles, nucléaire**) afin d’envisager une évolution possible de  notre  système  énergétique.  Une  évaluation  de  ses  impacts  socio-économiques  et  environnementaux  est réalisée a posteriori.

# Décourvrir
**Découvrir rapidement les points-clés de ce nouveau scénario de transition énergétique pour la France :**

[Les 10 points-clés du scénario négaWatt 2017-2050](https://negawatt.org/IMG/pdf/scenario-negawatt_2017-2050_essentiel-4pages.pdf)
1. Une division par 2 de la consommation d’énergie
2.  Le maintien d’un haut niveau de services énergétiques pour l’ensemble des besoin
3.  Une consommation couverte à 100 % par des énergies renouvelables en 2050 
4.  Une fermeture progressive du parc nucléaire
5.  La disparition du pétrole, du gaz fossile et du charbon
6.  Une mutation des pratiques agricoles et sylvicoles
7.  Gaz et électricité, une complémentarité nouvelle et incontournable
8.  La neutralité carbone pour la France en 2050
9.  Des bénéfices multiples pour la santé et l’environnement
10. La transition énergétique, un bienfait pour l’économie et l’emploi

[Vidéo : Réussir la transition énergétique en France avec le scénario négaWatt](https://www.youtube.com/watch?v=jXIaQLCVB2M&feature=youtu.be)
La transition énergétique est possible et souhaitable : les experts de l’Association négaWatt le montrent dans leur scénario 2017-2050 pour la France.

[Le chiffre de la semaine](https://negawatt.org/scenario-negawatt-le-chiffre-de-la-semaine)
Depuis la publication du scénario, l’Association négaWatt publie progressivement les chiffres-clés de ce scénario sur les réseaux sociaux.
# Comprendre
Comprendre les grandes lignes du scénario négaWatt par secteur de consommation et de production d’énergie, mais aussi ses principaux impacts socio-économiques et environnementaux:[ Les grandes lignes du scénario négaWatt 2017-2050 - pdf 12 pages](https://negawatt.org/IMG/pdf/scenario-negawatt_2017-2050_brochure-12pages.pdf)

Comprendre ce que recouvre concrètement la sobriété dans le scénario négaWatt: [La sobriété énergétique : pour une société plus juste et plus durable - pdf 12 pages](https://negawatt.org/IMG/pdf/sobriete-scenario-negawatt_brochure-12pages_web.pdf)

# Approfondir
**Approfondir la méthodologie, les hypothèses et les différents choix qui ont orienté la construction du scénario négaWatt :**

[Synthèse du scénario négaWatt](https://negawatt.org/IMG/pdf/synthese_scenario-negawatt_2017-2050.pdf)
Présentation détaillée de la méthodologie du scénario et de ses résultats - pdf 48 pages

[Hypothèses et résultats du scénario négaWatt](https://negawatt.org/IMG/pdf/scenario-negawatt_2017-2050_hypotheses-et-resultats.pdf), pdf 49 pages

[Graphiques dynamiques - outil en ligne](https://negawatt.org/scenario/)
Présentation de l’évolution de la consommation et de la production d’énergie dans le scénario négaWatt, secteur par secteur et filière par filière ainsi que les émissions de gaz à effet de serre associées, année par année jusqu’en 2050.

# Toutes les vidéos
[Plusieurs vidéos de présentation du scénario négaWatt sont accessibles librement.](https://negawatt.org/Video-presentation-complete-du-scenario-negaWatt), Durée : de 1h à 2h50 pour la première présentation officielle du scénario

[Webinaire sur le scénario négaWatt](https://www.youtube.com/watch?v=9cbbzd5ygwM&feature=youtu.be), Durée : 1h06

[Présentation du scénario aux Assises de la transition énergétique](https://www.youtube.com/watch?v=yl9PUL6SG3E&feature=youtu.be), Durée : 1h27.

[Intégrale de la première présentation du scénario à Paris](https://www.youtube.com/playlist?list=PLl7a-AwQmiveqy-tmRJM8ir5EtuOHby2z), Durée : 3h53 (divisé en grandes thématiques)

# Agenda
[Agenda négaWatt](https://negawatt.org/Agenda)
Cet agenda rassemble l’ensemble des interventions de l’Association négaWatt prévues durant les prochaines semaines.

# Association  négaWatt
L'Association négaWatt est **née  en  2001**  de la volonté d’un collectif d’experts indépendants et de praticiens de l’énergie, de promouvoir en France un système énergétique plus soutenable. Elle s’appuie aujourd’hui sur un réseau de plus de **1000 adhérents soutenant ses actions**.
* Les ressources  financières de cette  association à but non lucratif proviennent essentiellement des cotisations et des dons de ses membres, mais  aussi de contributions provenant de mécènes, de fondations ou d’ONG. 
* La réalisation et la diffusion du **scénario négaWatt 2017-2050** ont été largement soutenues par une campagne de financement participatif lancée en mars  2016, ainsi que  par la  Fondation Charles-Léopold Mayer pour le Progrès de l’Homme. 
* Le  scénario  négaWatt,  à  l’image  de  l’ensemble  du  travail  de  l’association,  est  le fruit d’un travail collectif, essentiellement  bénévole,  rendu possible par  la  mise  en commun de l’expertise des membres de l’association dans de multiples domaines.  

➡ Site : www.negawatt.org

# Institut négaWatt
L’Institut négaWatt, filiale opérationnelle de l’association, **a été créé en 2009**. Il a pour vocation de préparer et d’accompagner la transition énergétique dans les territoires, en s’appuyant  sur  les  propositions  du  scénario  négaWatt.  **Labellisé  «  entreprise solidaire  **»,  l’institut  est  un  incubateur  de  projets  pilotes  en  matière  de  transition énergétique. Il développe notamment depuis plusieurs années, dans une approche partenariale,  un  dispositif  opérationnel  de  rénovation  énergétique  des  maisons individuelles baptisé DORéMI. 

➡ Site : www.institut-negawatt.com


