<!-- TITLE: Le Pays du Bocage -->

# Présentation du Pays du Bocage
![Carte Pays Du Bocage Small](/uploads/test/carte-pays-du-bocage-small.png "Carte Pays Du Bocage Small"){.align-center}
# Historique
> A COMPLETER
# Les portails thématiques
* [L'agriculture et les circuits courts dans le Pays du Bocage](/france/normandie/orne/pays-du-bocage/agriculture-et-circuits-courts)
* L'énergie dans le Pays du Bocage
* La santé et les services à la personne dans le Pays du Bocage
* La transition numérique dans le Pays du Bocage
* L'attractivité du territoire dans le Pays du Bocage
* L'économie circulaire et locale dans le Pays du Bocage

# Les groupes de réflexion
* [L'habitat intergénérationnel dans le Pays du Bocage](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel)
# Voir aussi
## Pages connexes
## Liens
## Références
* [La candidature du PETR au fond LEADER pour la période 2014-2020](https://drive.google.com/file/d/109ZF3SiQNMGAJnGf6xLSLCVhgPK3QcQO/view?usp=sharing)


