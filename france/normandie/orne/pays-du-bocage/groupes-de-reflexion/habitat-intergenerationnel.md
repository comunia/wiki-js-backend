<!-- TITLE: Groupe de réflexion sur l'habitat intergénérationnel -->
<!-- SUBTITLE: Pays du Bocage -->

# Présentation
> A COMPLETER
# Historique des réflexions
* **27 Novembre 2018** : [Rencontre de lancement du groupe de réflexion](/france/normandie/orne/pays-du-bocage/gr-habitat-intergenerationnel/resume-27112018)
*  **7 Janvier 2019**  [Deuxième Rencontre](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel/deuxieme-rencontre%60)
*   **26 Février 2019**  [Troisième Rencontre](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel/troisieme-rencontre)
* **14 mai 2019**    [Quatrième Rencontre](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel/quatrieme-rencontre)
*  **25 juin 2019**  [Cinquième Rencontre](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel/cinquieme-rencontre)
# Etat des lieux
> A COMPLETER

# Cartographie des acteurs clefs
> A COMPLETER

# A voir aussi
## Pages connexes
* [Page thématique sur l'habitat intergénérationnel](https://wiki.territoires.co/sante-et-services-a-la-personne/habitat-intergenerationnel)