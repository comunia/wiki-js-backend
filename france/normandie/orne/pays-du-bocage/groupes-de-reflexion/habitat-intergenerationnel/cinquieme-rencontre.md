<!-- TITLE: Résumé de la cinquième rencontre sur le projet d'habitat participatif/intergénérationnel -->
# La rencontre
Cette cinquième rencontre s’est tenue au [Coliving El Capitan](https://www.facebook.com/coliving.elcapitan/), situé au Lieu-dit La Marchandière dans le village des Tourailles **mardi 25 juin 2019**. Les personnes suivantes étaient présentes : Marc C. (menuisier retraité, Chênedouit),  Bernard E. (professeur retraité, Saint-Paul), Marie E. (directrice d'IME, Saint-Paul), Marie W. (psychomotricienne, Argentan), Christine C. (retraitée, Falaise), Noelle P. (adjointe au maire, La Ferté Macé),  Franck Gauquelin (professeur de maths, résidant à la Marchandière), Rémi Gauquelin (en mission au pôle urbanisme de Flers Agglo, résidant à la Marchandière) , Hélène V. (membre de Territoires en Commun, résidant à Paris) et Aline M. (membre de Territoires en Commun, résidant à Paris).

![Maison](/uploads/test/maison.jpg "Maison"){.align-center}

La discussion s’articulait autour de la préparation de rencontres publiques sur l’habitat participatif, qui seraient organisées en automne prochaine sur le Pays du Bocage. 

# Ordre du jour
1. Présentation des idées identifiées lors du groupe de travail.
2.  Discussion autour des pistes présentées.
3.  Définition des prochaines étapes. 

# Les points clefs
## Objectifs de ces réunions publiques

* Montrer une image réelle de l’habitat participatif (s’adresse à tous : tout âge, tout modèle familial, toute nationalité, tout handicap, tout statut, tout milieu, etc.)
* Voir s’il y a un écho ou si le projet suscite de la curiosité parmi les citoyens et/ou les élus et/ou les bailleurs, etc.

## Logistique et contenu envisagés pour ces réunions publiques 

* **Dates**:  Un après-midi à l’automne. Dates envisagées : **16, 23 ou 30 Novembre 2019.**

*  **Format en plusieurs temps** :  
1. **Une présentation** avec support Power-Point pour expliquer clairement ce qu’est l’habitat intergénérationnel et participatif. Clarifier les différentes formules. Intégrer des vidéos de témoignages (résidents, monteurs de projet, etc. Cf 1er CR, liens disponibles). Faire venir des intervenants.
	*Invités envisagés* : 
		Pascal Gourdeau, *membre de l’ARDES, accompagnateur de projet d’habitat participatif et un des membres fondateurs des Z'ecobatisseurs à Caen*.
		Hélène Devaux, *psychosociologue qui accompagne l’émergence de projets d’habitats participatifs et monte progressivement un réseau sur cette thématique en Normandie, et    coordinatrice d’une résidence intergénérationnelle en projet à la Grand’Mare à Rouen (Cf [4ème CR](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel/quatrieme-rencontre)).*
2. **Tables rondes** organisées autour de thématiques, soit par type d’habitat, soit par type de porteur de projet (citoyen, bailleur social, commune…) ou autre.
3. **Restitution plénière**
4. **Dîner partagé, musique, animations** (nous envisageons de solliciter le MRJC de La Ferté Macé).

=> La réunion publique peut notamment être l’occasion de diffuser le questionnaire visant à cibler les besoins et envies des citoyens.

=> Dans cette démarche, nous nous présentons comme facilitateurs de projet pour mettre en lien différentes personnes et/ou organismes.
 
## Lieux envisagés pour ces réunions publiques

Plusieurs lieux ont été évoqués afin de couvrir le territoire du Pays du Bocage. Après discussions, deux lieux ont été retenus :

- **La Ferté Macé** : Noëlle P. y étant actuellement maire adjointe, elle se propose d’y réserver la salle des mariages pour y organiser la réunion publique. Le projet d’un quartier à réhabiliter étant déjà enclenché à La Ferté Macé (cf CR n°4), et la question de l’intergénérationnel déjà présente, il semble intéressant d’y proposer une réunion publique en 2019. De plus, nous envisageons de mobiliser les jeunes du MRJC, très actifs à La Ferté Macé, pour proposer des animations aux enfants lors de cette rencontre.

-  **Domfront** : On trouve de nombreuses dynamiques et initiatives dans le Sud du Bocage (Pub le Secret Knight, accueil de migrants, camembert du Champ secret bio et AOP, association des compagnons du sentier (randonnées jusqu’au Mont St Michel), etc.). Un projet d’éco-hameau a d’ailleurs été lancé, il y a quelques années à Champsecret (à 9 km/10 minutes de Domfront). Le projet s’est arrêté, mais des îlots sont désormais en vente à l’emplacement de ce projet. Le PETR du Pays du Bocage se trouvant à Domfront, nous pourrions solliciter leur aide pour trouver un lieu.

Pour couvrir d’autres zones du Pays du Bocage, des réunions publiques pourraient être organisées dans un deuxième temps dans les villes suivantes : 
-  **Flers** : La salle Jean Chaudeurge de la médiathèque de Flers pourrait être un lieu potentiel de rencontre.
-  **Putanges-Le-Lac** : cette zone du pays du Bocage est un vivier d’initiatives déjà très dynamiques avec des projets naissants (Le K-Rabo à Rabodanges ([k]oncert, [k]ommerce, [k]antine, [k]afé), Le hangar de Bréel (espace de coworking), etc.) où la mobilisation citoyenne est actuellement sollicitée. 

## Organisation des groupes de travail 
Trois pôles, tous inter-liés, ont été déterminés pour préparer ces réunions :

**Le pôle format** (contenu de la présentation, etc.) :
=> Membres : Marie E., Aline M., Marie W. (mais absente en août, skype ?) avec Hélène V. en appui. 
=> Document de travail :  [Lien](https://docs.google.com/document/d/1EPTBKG7XNO2KbUxt0LXojmFqLGx-ILEecoWmgZn6IAg/edit)

**Le pôle relais communication** :
o Potentiels canaux de diffusion : réseaux sociaux de partenaires (coliving, K-rabo, ferme de la Berouette, etc.), familles rurales, écoles, commerces, journaux, associations, biocoop, agences immobilières, etc.
o Trouver une phrase d’accroche à adapter à chaque canal de communication.
=> Membres : Christine C., Aline M., Marie W. (mais absente en août, skype ?).
=> Document de travail : [Lien](https://docs.google.com/document/d/1zkq6VyYOxE1SuUlyLs7r6zg1I1mZmlh5dAPWfTJGay0/edit)

**Le pôle logistique** (trouver les lieux, contacter les intervenants, etc.) : 
Membres : Noëlle P. (trouver lieu à La Ferté Macé), Aline M. (trouver lieu à Domfront) et pour tout le monde (contacter intervenants : MRJC, P Gourdeau et H Devaux ?).
 => Les lieux investis pour les différentes réunions publiques doivent pouvoir accueillir des ateliers.

**Autres propositions** ; Hélène V. nous suggére de  :
 - Consulter le site de [La Fabrique](https://www.colibris-lafabrique.org) qui recense tous les oasis de France (qui comporte des vidéos et notamment d’un collectif intergénérationnel).
 - Le site du [mouvement colibris](https://www.colibris-lemouvement.org/) où l’on peut également trouver des vidéos d’oasis, et le contact d’une dizaine de consultants sur l’habitat participatif.
- Contacter les Colibris pour leur proposer d’intervenir.
# Calendrier
Prochaines dates à retenir pour préparer les réunions publiques :
**- 4 août**: Groupes de travail format & communication chez Marie & Bernard E.
**- 23 août** : Groupes de travail format & communication chez Marie & Bernard E.
**- 19 septembre** : 6ème rencontre à partir de 19h (lieu à définir : coliving?).
 
Autres dates à garder en tête (notamment pour la communication) :
**- 30 août** : forum des associations à La Ferté Macé.
**- 6 septembre** : festival de la Revoyure à La Ferté Macé.
**- 7 septembre** : forum de la rentrée à Flers.
**- 11 et 12 octobre** : rencontre de l’habitat participatif à Caen.

# Actions à mener
### 1- Débuter les réflexions pour l'organisation des rencontres publiques
- Première réfléxions groupe de travail format et le communication avant le 4 août

### 2- Prendre contact avec les invités/partenaires et réserver les lieux (avant septembre)

### 3 - Lancer la communication &  finaliser le format de présentation (septembre)

### 4- Retravailler le questionnaire  (Marie W. (avec l’aide de Adrien D.) en septembre)


