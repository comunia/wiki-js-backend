<!-- TITLE: Résumé de la Deuxieme Rencontre sur le projet d'Habitat intergénérationnel -->

# La rencontre 
Cette deuxième rencontre s’est tenue à Saint Paul le  lundi 7 janvier 2019. Les personnes suivantes étaient présentes : Monique (directrice retraitée d'un centre de santé, Val d'Orne), Bernard E. (professeur retraité, Saint-Paul), Igor L. (membre de Territoires en Commun, Chênedouit), Marie E. (directrice d'IME, Saint-Paul), Marie W. (psychomotricienne, Argentan), Christine C. (retraitée, Falaise) et Aline M. (membre de Territoires en Commun, résidant à Paris).

La rencontre était organisée autour d'un repas partagé chez Bernard et Marie dans leur maison à Saint Paul. La discussion s’articulait autour d'un compte rendu, réalisé par Marie W. et Aline M., présentant des échanges qu’elles ont pu avoir sur des expériences d'habitat intergénérationnel.

![20190107 2 Eme Rencontre Habitat Intergenerationnel 2](/uploads/habitat-intergenerationnel/20190107-2-eme-rencontre-habitat-intergenerationnel-2.jpg "20190107 2 Eme Rencontre Habitat Intergenerationnel 2"){.align-center}
# Ordre du jour
1. Présentation de l'initiative "Territoires en Commun"à Christine et de ses avancées (voir [le livre blanc](/livre-blanc) pour plus d'informations) 
2. Présentation de Marie W. d’un exemple d’habitat intergénérationnel à Lyon, suite à une discussion avec Robin Gary, résident de la structure.
3. Présentation d’Aline M . de conseils sur la démarche de création de projet d’habitat intergénérationnel, suite à une discussion avec Alexandre Faure, fondateur de Sweet home, blog présentant des réflexions autour du problème de la préservation de l’autonomie des personnes âgées.
4.  Discussion ouverte sur les pistes à mener.

# Les points clefs

## Fonctionnement de la Maison intergénérationnelle de Lyon 
**Porteur de projet** :  [Entreprendre pour Humaniser la Dépendance](https://www.habitat-humanisme.org/le-mouvement/entreprendre-pour-humaniser-dependance/) (SCIC).
**Gestion de la maison d’accueil**: assurée conjointement par le réseau  **[La Pierre Angulaire**](https://www.habitat-humanisme.org/la-pierre-angulaire/(Groupe Habitat et Humanisme) et par l’**Association Lyonnaise de Prévoyance** (ALP).
> Maison au centre de Lyon, rénovée de 2010 à 2014 pour assurer les infrastructures nécessaires (notamment pour les personnes handicapées) et lancer le projet de maison intergénérationnelle.

**Description:** 
* 70% de personnes âgées & 30% d’étudiants (dans le social et le paramédical) qui ont chacun leur appartement mais existence d’espaces commun partagés.
* Loyer bas pour les étudiants, plus élevés pour les personnes âgées
* Une personne gère le recrutement des nouveaux arrivants
* Pas d‘obligation mais contrat moral de prendre soin des uns & des autres
* Autogestion des résidents pour lancer des initiatives (fêter les anniversaire, groupe whatsapp entre jeunes résidents pour veiller sur les personnes âgées, etc.)
* Tous les 6 mois, une Assemblée Générale a lieu pour pointer ce qui va et ce qui va moins bien
* Plus récemment, ouverture du lieu à des publics différents de ceux ciblés à l’origine (famille syrienne, jeune femme tétraplégique, etc)

> Pour plus d'information : Voir le [CR complet de l'échange](https://drive.google.com/file/d/1gabPNMW9yDtswu5DaVNBwfuNTrqnNf_d/view?usp=sharing)

**Points importants à relever**: 
* L’implication volontaire des résidents contribue au maintien d’une bonne cohésion au sein de la maison. Le recrutement des nouveaux résidents ne doit donc pas être négligé. 
* Le fait d’avoir chacun son appartement et des espaces communs partagés permet de maintenir une certaine intimité “un chez soi” tout en disposant d’espaces d’échanges quand les résidents le souhaitent. 
* Le modèle fianncier (montant des loyers en fonction du résident) est un point à creuser (ex: Quel est le montant du loyer si le résident n’est plus étudiant?)
* Les aspects contractuels sont gérés par une agence.

## Points importants lancement d’une démarche de création habitat intergénérationnel 

* Le facteur de succès clef est l’envie des membres, qui se traduit par une **réelle volonté des différents parties de faire quelque chose ensemble**. Il faut donc s’entourer de personnes souhaitant s’impliquer dans ce projet pour réussir la démarche.
* Les projets sont souvent fragiles, un élément perturbateur peut faire effondrer l’ensemble du projet (ex: Logements sociaux alloués à des personnes n’étant pas initiallement dans le [projet Babagaya à Montreuil](https://www.lamaisondesbabayagas.eu/)
* Le schéma financier doit être robuste. A noter, certaines aides peuvent être cumulées (ex: Aides pour personnes handicapées , [Club des 6](https://sweet-home.info/habitat-collectif/propositions-habitat-partage-club-des-6-handicap-inclusif/)
* Public ciblé (hors personnes âgées) :
* Problématiques des jeunes, car ils ne restent souvent que peu de temps (stage de 6 mois) ou non 100% du temps (alternance). Ce qui mène à une implication non pérenne. Donc cibler plutôt jeunes actifs isolés ou famille

# Proposition de démarche

**1. Commencer par cibler le besoin du territoire**
**Identifier personne âgées**  et échanger sur leurs envies/besoins : se verraient elles vivre avec une famille ? en colocation ? quels problèmes rencontrent-elles (dépendance,
voiture), etc. 
Les contacter via les club d’anciens, les universités interage etc, mais également se mettre en lien avec les associations d’aide à domicile, qui sont au contact des personens agées.

> Pour ces échanges, il a été suggéré d’établir **un petit questionnaire**

**2. Cibler dans un premier un type de solution**
Bien ciblé également la zone (très différent si l'habitat se fait en milieu rural ou en mileu urbain)

**3. Trouver des personnes intéressées par ce type de solution et travailler ensemble au montage du projet**

**4. Si la démarche marche, on peut élargir à d’autres types de solution**

# Actions à mener
## 1. Contacter des personnes en lien avec l'habitat intergénérationnel
* Contacter Claire Dutilloy & Jean Pierre Derret (voir [Article](/lhttps://actu.fr/normandie/alencon_61001/un-projet-dhabitat-participatif-et-intergenerationnel-sur-les-rails_7212147.html), projet d’habitat intergénérationnel à Alençon en 2016 (Aline)
* Contacter la structure gestionnaire de la maison intergénérationnelle à Lyon pour comprendre le modèle financier et diffiicultés rencontrées (Marie W)
* Rencontrer Paul Alexis Racine (association CETTE famille) https://www.cettefamille.com/, (Aline & Igor)
* Contacter l'Association L.I.E.N., Cohabitation intergénérationnelle en Basse-Normandie : http://associationlien.fr/ (Marie E)
* Proposer aux personnes contactées de venir témoigner lors de la prochaine rencontre (PA Racine , Claire Dutilloy, Asso à Caen, etc )

## 2. Rassembler de la documentation sur les points suivants :
* Se renseigner sur les habitats intergénérationnels à Caen ( [Z'ecobatisseurs](https://leszecobatisseurs.wordpress.com/), éco quartier Louvigny; Colombelles, bailleur social  les foyers Normands, Projets à Caen en 2019 & 2020 etc)
* Se documenter sur l’habitat intergénérationnel via le Blog d’Alexandre Faure https://sweet-home.info/category/habitat-collectif/
* Se renseigner sur l'Association CoSI

 ## 3. Commencer à travailler sur l'élaboration d'un questionnaire 
* Réfléchir à de potentiels élus pouvant être réceptifs à ce type de projet, comme la commune d’Athis 
* Elaborer un questionnaire à destination des personnes âgées du territoire pour comprendre leurs besoins
* Réfléchir à une stratégie de diffusion de ce ce questionnaire (quelle ville? combien de personnes? comment? via les clubs des anciens, les association d’aides à domiciles, les université interages, etc)



