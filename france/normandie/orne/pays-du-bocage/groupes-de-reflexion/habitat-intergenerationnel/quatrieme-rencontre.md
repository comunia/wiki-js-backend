<!-- TITLE: Résumé de la quatrième Rencontre sur le projet d'Habitat intergénérationnel -->

# La rencontre 
Cette quatrième rencontre s’est tenue à La Ferté Macé mardi 14 mai 2019. Les personnes suivantes étaient présentes : Hélène D. (Accompagnatrice projets habitat participatif, Rouen), Marc C. (menuisier retraité, Chênedouit), Igor L. (membre de Territoires en Commun, Chênedouit),  Bernard E. (professeur retraité, Saint-Paul), Marie E. (directrice d'IME, Saint-Paul), Marie W. (psychomotricienne, Argentan), Christine C. (retraitée, Falaise), Jacques D. (maire, La Ferté Macé), Noelle P. (adjointe au maire, La Ferté Macé), Adrien D. (membre de Territoires en Commun, résidant à Paris)  et Aline M. (membre de Territoires en Commun, résidant à Paris).

La rencontre était organisée en deux temps :
une visite des îlots de la Ferté Macé (projet habitat participatif)
un repas partagé chez Noëlle dans sa maison à La Ferté Macé.

La discussion s’articulait autour du projet participatif des îlots de la Ferté & Macé, des expériences d’accompagnement d’Hélène sur les projets d’habitat participatif et les pistes à mener pour continuer le projet (questionnaire et/ou rencontre publique)

# Ordre du jour
1. Visite  & Discussions autour du projet d’habitat participatif en étude à la Ferté Macé 
2. Présentation du questionnaire réalisé par Marie W. & échanges autours des actions envisagées pour sa diffusion
3. Discussion ouverte sur les pistes à mener

# Les points clefs
## Visite & Discussions autour du projet d’habitat participatif en étude à la Ferté Macé 

Nous avons débuté cette rencontre avec la visite des îlots Chauvière et Marcel Pierre. Ces îlots sont situés à côté de la mairie, au coeur de la ville de la Ferté Macé.

Les Îlots Chauvière sont composés de plusieurs immeubles avec au rez-de-chaussée les locaux d’une ancienne pharmacie. Les locaux du RDC sont occupés partiellement par la structure du Centre de Développement pour l’Habitat et l’Aménagement des Territoires ([CDHAT](https://www.cdhat.fr/domaine-d-intervention/particuliers/amelioration-de-l-habitat)) qui informe et accompagne gratuitement dans votre démarche les personnes souhaitant bénéficier d’aide pour la rénovation de leur logement (Opération Programmée d’Amélioration de l’Habitat (OPAH)). Le reste des bâtiment est quasi vide : sur les 8 propriétaires de la vingtaine de logements des îlots Chauvière, seul un est occupant.
L’espace est très grand (chaque bâtiment comporte plusieurs appartement), et nécessite de travaux de rénovation. 

Les îlots Marcel Pierre appartiennent à la ville et sont composés de l’ancienne maison de Marcel Pierre et d’autres bâtiments avec terrain vert. Après de nombreux squats qui ont dégradé l’intérieur du bâtiment, les portes & fenêtres du bâtiment ont été condamnées. 

**Etude de faisabilité**
La Ferté-Macé a pu obtenir un appui financier de l’Europe (fonds LEADER) et de Flers Agglo pour financer une étude de faisabilité auprès d’un cabinet d’architecte afin d’évaluer le potentiel de transformation de ces habitats. Les termes de référence insistaient sur deux variables : une variable “sociale” et une variable “sociable”. La variable sociale visait à faire de ces logements des espaces accessibles au plus grand nombre tandis que la variables sociables visait à transformer ces habitats inoccupés (pour la grande majorité) en espaces créateurs de lien social.
Les premiers résultats de l’étude ont permis de repenser l’utilisation des îlots pour en faire des logements sociaux, répondant ainsi à la variable sociale mais n’ont pas intégré la variable sociable. La ville aurait souhaité qu’une réflexion soit menée sur la création d’espaces collectifs (jardins partagés, espaces associatifs, mise à disposition d’espaces pour des commerçants en phase de lancement, etc.). Une réunion de restitution a ainsi eu lieu en mars dernier. 

**Les suites** :
Regarder s’ils existent des initiatives similaires en France (projet d'habitat participatif porté par une commune)


## Questionnaire
Marie W. a réécrit le questionnaire afin qu’il soit diffusable à un plus grand nombre (pas seulement auprès personnes âgées) :  [lien](https://docs.google.com/document/d/0B102Ox1GyJDvQ1NULUU1MHgtWVdsM0RJcW5yQmEzVGlPS2t3/edit#). Le questionnaire a été testé auprès de Marc C. mais s’avère trop long (une vingtaine de minutes). L’idée serait donc de le scinder selon le public ciblé. Adrien D. s’est proposé pour adapter le format afin que l’analyse des réponses soient exploitables.

Le périmètre de nos réflexions étant large (Pays du Bocage), nous nous sommes interrogés sur le périmètre et les moyens de diffusion. Il a été suggéré que ce questionnaire ne soit diffusé que dans un second temps et en proposant de le remplir avec la personne interrogée dans le cadre d’un réel échange. 

L’idée serait de réaliser dans un premier temps **des rencontres publiques pour présenter l’habitat participatif à différentes communes**. Cela permettrait de présenter les différentes formes d’habitat participatif, de voir qui serait intéressé par ce type de projet (communes et citoyens) et  ainsi de mieux cibler le périmètre de la démarche.

## Autres points
* Agenda : *11 & 12 octobre Rencontre Habitat Participatif  à Caen*.
* Hélène anime la page facebook du **Réseau pour l’Habitat Participatif Normand** https://www.facebook.com/ReseauHPN/
* **Habitat Participatif France** http://www.habitatparticipatif-france.fr/?HPFCartographie 
* **La Bascule** : Collectif citoyen basé dans une ancienne clinique désafectée à Pontivy (lieu éphémère). Un chantier participatif a permis de transformer les 6000 m² en quartier général pour ce mouvement de lobbying citoyen. Le collectif peut rester dans ces locaux pendant 6 mois. Il existe également des cellules locales.  Igor s’est mis en contact avec eux pour voir s’ils étaient à la recherche d’un (nouveau) lieu et parler des îlots de la Ferté.  https://la-bascule.org/


#  Actions à mener
## 1- Contacter des personnes en lien avec l'habitat intergénérationnel et continuer la veille documentaire
* Nicolas Knapp, architecte (avec François Versavel) du projet des Z’écobatisseurs à Louvigny près de Caen ? Olivier Sensiti,  économiste de Nantes, pouvant accompagner sur les questions de statuts & finances?
* La Bascule (Igor)
## 2- Réflexion suite projets îlots la Ferté Macé
* Regarder s’ils existent des initiatives similaire en France (projet d'habitat participatif porté par une commune).

## 3. Réfléchir à comment organiser les rencontres citoyennes pour présenter l’habitat participatif (logistique, format, date etc) 
* Création d’un petit groupe en amont pour préparer ce point (Aline, Marie W , Marie E)
