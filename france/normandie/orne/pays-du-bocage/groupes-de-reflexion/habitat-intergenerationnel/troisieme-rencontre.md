<!-- TITLE: Résumé de la troisième Rencontre sur le projet d'Habitat intergénérationnel-->

# La rencontre 
Cette troisième rencontre s’est tenue à Saint Paul le **mardi 26 février 2019**. Les personnes suivantes étaient présentes :  Christine C. (retraitée, Falaise), Bernard E. (professeur retraité, Saint-Paul), Igor L. (membre de Territoires en Commun, Chênedouit), Marie E. (directrice d'IME, Saint-Paul), Marie W. (psychomotricienne, Argentan), Jacques D. (maire, La Ferté Macé), Noelle P. (adjointe au maire, La Ferté Macé) et Aline M. (membre de Territoires en Commun, résidant à Paris).


![Habitat](/uploads/test/habitat.jpg "Habitat")

La rencontre était organisée autour d'un repas partagé chez Bernard et Marie dans leur maison à Saint Paul. La discussion s’articulait autour de compte rendus, réalisés par Marie E. et Aline M., présentant des échanges qu’elles ont pu avoir sur des expériences d'habitat intergénérationnel. Jacques D. & Noelle P. ont également pu partager le projet d’habitat participatif en étude à la Ferté Macé (Orne). 

# Ordre du jour
1. Présentation de Marie E. de ses recherches sur l'habitat participatif, notamment via le Réseau HPN (Habitat Participatif Normand)
2. Présentation d’Aline M. de la discussion avec Pascal Gourdeau des Z'ecobatisseurs à Caen
3. Présentation de la rencontre avec Jean Pierre Deret, porteur du projet d’habitat participatif à Alençon (Orne)
4. Présentation de Jacques D. & Noelle P. du projet d’habitat participatif en étude à la Ferté Macé (Orne)
5. Discussion ouverte sur les pistes à mener 

# Les points clefs

## Veille sur l’habitat participatif 

**Formation en ligne** 
* Les Colibris : formation en ligne gratuite : Cours en ligne “MOOC concevoir une oasis” sur l’émergence/création collectif/montage technique (25 vidéos, 3 parcours, 17 modules).

**Sites internet**
* http://www.habitatgroupe.fr/  :  rendre visible les démarches. Mettre en lien les porteurs de projet (projet Coutances & Mortagne au Perche).

* http://www.hameaudesbuis.com  vers Montelimar (ecohammeau intergenerationel & lieu de vie qui associe l’école "la Ferme des Enfants" et des habitations sur un espace vivrier agricole). Il propose notamment 6 modules courts d’écoformation.
  
* Association de Développement de l'Économie Sociale et Solidaire (ADESS) : Recensement des habitats groupés français.
 
* http://babalex.org/ ou http://www.habicoop.fr  : questions juridiques de l’habitat groupé.
  
* http://www.toitsdechoix.com  : Accompagnement de collectifs pour réaliser des projets d’habitat groupé écologique en autopromotion (formation).
 

**Appels à projets**
* [Association Régionale pour la Promotion de l’Eco-construction en Normandie](http://wp.arpe-bn.com/2019/01/09/appel-a-projet-ouvert-pour-location-et-habitat-groupe-et-participatif/?fbclid=IwAR2zaqgGkGjpE6Yg7YfPjiin-i01nY5dnVypbnFC3nfFD3_F83AW1Fgoh4Y) 

* [Grand prix de l’ESSEC de la ville solidaire et de l’immobilier responsable](http://gdprix-villesolidaire.essec.edu/home?fbclid=IwAR1N8yXJGMLXl_g4xPga7uQbmBMZs_amiH-E-cNdhujnry68LIUkZ5BPVxw) 1ère édition remise de prix le 3 juin 2019

**Autres projets sur la Région**
* **Caen la Mer**
	* Projet de  résidence participative intergénérationnelle, animé par la Caen la mer Habitat, avec Bouygues et Réciprocité.
	* Résidence Philia : 103 logements dont foyer Léone Richet (malades psy) 
	* Film « les z'ecobatisseurs une utopie au m² » : 13 logements groupés à Louvigny.

* **Association Néologis**
*Espace des Vaillons 267 rue du Paris 76800 St Etienne de Rouvray, 06 70 57 00 94,  contact@neologis.org*
	* Objectif principal:  créer, améliorer et gérer de manière participative des logements destinés aux ménages aux revenus modestes ne trouvant pas de réponse sur leur territoire concernant leur habitat. 
	* Anime le réseau HPN (réseau pour l’Habitat Participatif Normand) https://www.facebook.com/ReseauHPN/ 
	* Accompagne projet habitat participatif et intergénérationnel à Rouen.
	* L'association BVGM (Bien Vivre et Vieillir à la Grand Mare) pour un projet d'habitat innovant, solidaire et intergénérationnel à la Grand Mare (Rouen)
	* Réseau COSI fédère et outille 26 structures + COSI expertise.
		*  Construction de logements locatifs dans l’ancienne mairie de Saint-Jacques-sur-Darnétal
		*  Commune met à disposition des locaux -> bail à réhabilitation.
		*  Emprunts pour travaux Néologis -> logements locatifs sociaux -> loyers remboursement le prêt.
		*  Accompagnatrice projet habitat participatif :  Hélène Devaux 06 62 26 16 00 (2j ½) psychosociologue spécialisée en réhabilitation urbaine
		*  Maison des Solidarités à Caen (Pascal Gourdeau)

## Pascal Gourdeau des Z’écobatisseurs (Caen)
**Projet en cours à Louvigny près de Caen suivi par Pascal Gourdeau**
La ville de Louvigny compte 2800 habitants. 3 espaces d'habitat participatif sont en construction : un proche des commerces, un plus éloigné et un près des écoles (afin de répondre aux différents attentes des habitants de Louvigny (famille, personnes âgées etc)). 2 sites sur 3 seront accessibles à pied ou en transport en commun. 

> Un questionnaire a été réalisé et diffusé via la mairie à Louvigny (CCAS) pour comprendre & analyser les besoins de la population. (Voir [Questionnaire](https://drive.google.com/file/d/0B102Ox1GyJDvWkg0RGZnOVYzWTVHZ0JKa1BNanlPVGV6SDBn/view?usp=sharing))

**Conseils Pascal Gourdeau**
Quand on monte un projet d’habitat participatif, il faut regarder le Plan Local d’Urbanisme (PLU) & le Plan Local d’Habitat (PLH) dans les villes. 
Il est également important de répondre aux interrogations suivantes :
* “Je suis loin”  : Comment rendre l’habitat plus accessible?
* “J’ai envie d’être proche des gens” : Il faut que les personnes aient envie d’être dans le projet. Il y a un gros travail de médiation/education populaire à réaliser en amont.
> La clef du succès est dans la combinaison du *“j’ai besoin & j’ai envie”*

On peut également travailler sur les fondamentaux (ex: isolement) ou via des ateliers (ex : construction du plan de masse du futur habitat participatif avec des legos). 
>Ce travail en amont de sensibilisation et de mobilisation des personnes prend du temps mais est nécessaire pour que le projet marche.

Un groupe de 3 experts s’est formé pour accompagner techniquement les projets d’habitat participatif avec :
* Nicolas Knap, architecte biodynamique vivant à Falaise
* Olivier un économiste de Nantes, pouvant accompagner sur les questions de statuts & finances
* Pascal Gourdeau, adminstrateur à l'ARDES et habitant au Z'écobatisseurs

> Se tiendra le 12 octobre 2019, la 3eme rencontre sur l’habitat participatif

## Projet d’habitat participatif de Jean Pierre Deret (Alençon)
**Historique du projet** : Une réunion publique sur l’habitat participatif s’est tenue en avril 2016 à la maison de la vie associative suite à un appel à participation dans un journal. Une vingtaine de personnes se sont montrent intéressées.
3 groupes se sont formés selon les attentes des participants: un habitat dans l’hypercentre, un habitat en ville incluant des espaces jardin (porté par JP Deret) et un espace en périphérie (porté par Claire Dutilloy).  Le projet porté par Jean-Pierre Deret est toujours en cours et en discussion via un groupe de réflexion. 

> Point intéressant : Les jeunes familles sont plus intéressées par un espace à la campagne tandis que les jeunes retraités s’imaginent plus dans le centre (pour assurer un accès aux commerces de proximité & faciliter leur mobilité).

**Groupe de réflexion** : Ce groupe est composé de 4 couples de retraités (incluant Jean Pierre & Catherine Deret), qui se réunissent chaque mois pour discuter du projet.
Leur point commun :
* Tous souhaitent vendre leur maison pour acheter un nouvel espace plus petit incluant des espaces communs.
* Ces couples de soixantenaires ne se voient pas dans quelques années dans une grande maison vide, certains ne sont pas basés à Alençon (Un couple est à 20km du lieu)
* Tous souhaitent construire un éco habitat. Ils rencontrent d’ailleurs le 21 février la société Ecopertyca et un architecte.

**Le projet** :  La ville d’Alençon propose un parc de 72 logements en périphérie d’Alençon (Porte de Bretagne proche de commerces dont magasin bio), pour 78euros le m2 (au lieu de 98). 1900m2 ont été réservés pour ce projet d'habitat participatif.
L’idée est de construire 4 à 5 maisons en bois ainsi qu’une maison commune avec chambre d’invités, salle de réunion, espace bricolage, jardin en commun.
Ils doivent rendre une décision en septembre/octobre pour se prononcer sur l’achat. La construction prendrait ensuite environ 2 ans

**Les questionnements**: 
* Ils doivent tous réussir à vendre leur maison pour pouvoir acheter le terrain.
* Ils n’ont aucune aides extérieurs.
* Un des couples n’est pas sûr de continuer l’aventure,  JP Deret pense lancer un nouvel article pour chercher d’autres intéressés.
* Ils se posent également des questions en tant que futurs propriétaires : Comment acheter ? Chacun sa maison ou via des parts partagés? Que faire en cas de décès? Comment remplacer quelqu’un qui part?

> L’appel à intéressés dans un journal peut être intéressant pour trouver des porteurs ou intéressés par cette aventure.
> La construction du projet prend du temps et le modèle juridique pose beaucoup de questions

## Projet d’Habitat Participatif (la Ferté Macé)
Les îlots de la Chauvière et Marcek Pierre se situent dans le centre ville de la Ferté Macé. Ils sont composés de 8 immeubles (une trentaine de logements pour une dizaine de propriétaire). Seul un propriétaire réside encore dans un des appartements.

La Ferté-Macé a pu obtenir un appui financier de l’Europe (fonds LEADER) et de Flers Agglo pour financer une étude de faisabilité auprès d’un cabinet d’architecte afin d’évaluer le potentiel de transformation de ces habitats en habitat participatif. 

Les termes de référence insistaient sur deux variables : une variable “sociale” et une variable “sociable”. La variable sociale visait à faire de ces logements des espaces accessibles au plus grand nombre tandis que la variable sociable visait à transformer ces habitats inoccupés (pour la grande majorité) en espaces créateurs de lien social.
 
> Une réunion de restitution du cabinet d’étude s'est tenu mardi 12 mars à la Ferté Macé (Voir [Article](https://www.ouest-france.fr/normandie/la-ferte-mace-61600/la-ferte-mace-les-proprietaires-veulent-davantage-d-informations-6264781

# Actions à mener
## Contacter des personnes en lien avec l'habitat intergénérationnel et continuer la veille documentaire

## Retravailler le questionnaire
* Retravailler le questionnaire pour l’orienter vers un plus public plus large (Voir [Questionnaire](https://docs.google.com/document/d/1w5nTZY3pVfTwczC2gjxFx9KgsZWH8Gqhgk-xOdIydI0/edit?usp=sharing))
*  Cibler des moyens & le périmètre de diffusion
## Organiser la prochaine rencontre
* Inviter Hélène Devaux à la prochaine rencontre, qui aura lieu le 4 avril
## Evénements
* Se rendre à la réunion de restitution du cabinet d’étude du mardi 12 mars à la Ferté. 

