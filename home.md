<!-- TITLE: Une plateforme collaborative pour construire les territoires de demain -->

# La démarche
Notre démarche vise à créer un espace de collaboration et des outils permettant aux différents acteurs d'un territoire de travailler de concert pour penser et construire le monde de demain. **Cette plateforme collaborative s'adresse aux acteurs suivants:**

**1. Collectivités et institutions :** les acteurs publics et institutionnels jouent un rôle moteur dans l'organisation et la planification d'un territoire. Certains disposent d'informations clefs pour comprendre des enjeux locaux alors que d'autres ont besoin de ressources pour alimenter leurs réflexions et orienter leurs politiques. Dans la plupart des cas, collectivités et institutions ne retrouvent à la fois dans la position "d'acteur ressource" (i.e. disposant d'informations à partager) et "d'acteur récipiendaire" (i.e. en demande d'informations).

**2. Acteurs académiques :** universitaires, centres de recherche et chercheurs indépendants produisent également une large quantité de données et d'analyses dont les conclusions pourraient être mises à profit de façon plus systématique. Nombre de ces travaux mériteraient d'être mis en lumière, traduits dans un language accessible et diffusés aux acteurs en position de s'emparer des conclusions tirées. 

**3. Entreprises et associations :** les acteurs privés ont également un rôle clef à jouer en partageant des perspectives de terrain essentielles à la bonne compréhension des enjeux et problématiques de développement territorial. Les principes de réalité auxquels ces acteurs sont confrontés représentent une clef de lecture qui échappe souvent aux acteurs institutionnels et académiques. Par ailleurs, ces acteurs de terrain sont généralement à l'origine des innovations qui façonnent notre société. Cette diversité d'informations mérite d'être traduite en contenu accessible et exploitable par la communauté.

**4. Citoyens et porteurs de projet :** les habitant.e.s d'un territoire sont in fine celles et ceux qui le façonnent, à travers leurs choix de consommation, leurs activités du quotidien, leurs choix politiques, leurs tendances de mobilité et l'énergie qu'ils décident de canaliser pour influer positivement sur le futur de leur communauté. A nouveau, ces différentes informations doivent être partagées afin d'être prises en compte par l'ensemble des acteurs travaillant à la construction des territoires de demain.

Cette plateforme s'inscrit dans une démarche plus globale que nous présentons dans un livre blanc disponible sous format illustré [ici]() et sous format web [ici](livre-blanc).

![Vallee Clecy](/uploads/vallee-clecy.jpg "Vallee Clecy"){.align-center}
# Les objectifs
* **La mise en commun d'informations, de données et de documents :** un nombre important de rapports, études, analyses prospectives, diagnostics, jeux de données et autres informations sont produites par une diversité d’acteurs. Toutefois, elles sont souvent stockées sur des sites dont la visibilité reste limitée. Cette plateforme vise à centraliser cette documentation, en améliorer la visibilité et la rendre plus facilement accessible. 

* **Le partage d'expériences , de bonnes pratiques et de projets prometteurs :** nos territoires renferment une richesse d’expériences qui mériteraient d’être mises en valeur. Ces expériences peuvent provenir d’acteurs institutionnels, de collectivités, d’associations ou d’entreprises. La plateforme permettrait de documenter ces expériences en présentant notamment l’historique de chaque initiative, le contexte de mise en oeuvre, les contacts pertinents, les leçons tirées, les travaux d’évaluation et toute autre information d’intérêt.
* **La mutualisation du travail d'analyse :** comprendre les enjeux actuels impactant nos territoires et les mutations à venir requiert un travail d'analyse mobilisant une diversité de perspectives de nature académique, institutionnelle, praticienne (i.e. acteurs de terrain) et citoyenne. Cette plateforme offre un espace permettant de faciliter : (i) l'organisation de savoirs pluridisciplinaires et pluri-thématiques ; et (ii) la déclinaison d'analyses globales à l'échelle territoriale (ex: déclinaison des Scénarios [négaWatt](energie/scenario-nega-watt-2017) et [Afterres2050](agriculture-et-circuits-courts/scenario-afterres-2050) à l'échelle de la Normandie).
# Les thématiques de travail
La démarche a été lancée autour d'une liste de 5 problématiques prioritaires (qui sera progressivement élargie à d'autres thématiques) :

1. [Agriculture et circuits courts](agriculture-et-circuits-courts): Comment nourrir sainement et localement la population du territoire tout en améliorant la biodiversité locale et à la qualité des sols?
2. [Energie](energie): Comment répondre aux besoins énergétiques du territoire de façon durable et respectueuse de l’environnement?
3. [Economie circulaire et locale](economie-circulaire-et-locale): Comment développer un modèle de production basé sur une faible consommation de matières premières, une faible consommation d’énergie et une production limitée de déchets?
4. [Santé et services à la personne](sante-et-services-a-la-personne): Comment améliorer la santé de la population du territoire, réduire les besoins en matière de santé curative et développer une offre de services adaptée à une population vieillissante? 
5.  [Attractivité du territoire](attractivite-du-territoire): Comment redynamiser nos territoires ruraux et inverser les tendances démographiques actuelles (de vieillissement de la population dans les zones rurales et d'urbanisation excessive dans les grandes métropoles)? 

# La méthode
> A COMPLETER
# Le Pays du Bocage : Territoire d'expérimentation
[Le Pays du Bocage](/france/normandie/orne/pays-du-bocage) est un territoire situé dans le département de l'Orne. Nous testons actuellement la méthode collaborative présentée ci-dessus avec une diversité d'acteurs de ce territoire (collectivités, institutions, entreprises, associations et citoyens). Les détails de ce projet de recherche collaborative sont présentés dans [ce document](/uploads/territoires-en-commun-projet-de-recherche-collaborative.pdf "Territoires En Commun Projet De Recherche Collaborative") (version web accessible [ici](france/normandie/orne/pays-du-bocage/projet-de-recherche-collaborative)). Nous identifions actuellement d'autres territoires pouvant intégrer cette expérience pilote. Cette liste sera publiée début 2019 sur [cette page]() et fera l'objet d'une actualisation progressive.
# Comment contribuer ?
> A COMPLETER
# Comment utiliser ce wiki ?
Démocratisé par [Wikipedia](https://fr.wikipedia.org/wiki/Wiki), le wiki est une famille d'application web permettant la production et la gestion collaborative de contenu. En pratique, cela signifie qu'il est possible pour toi, *ô lecteur*, d'y apporter des modifications et des ajouts afin d'enrichir la base de connaissance du projet de *tes lumières*. L'aventure te tente ? C'est parti !

Pour commencer, il nous faut hélàs préciser qu'afin de se prémunir des robots malicieux et des attaques venues du grand Internet, l'édition de ce wiki nécessite la création d'un compte et la demande de droits d'édition auprès d'un administrateur du wiki. La démarche à suivre pour ce faire est décrite sur la page [Création de compte](wiki/creation-de-compte)

Une fois cela fait, tu peux commencer à contribuer du contenu sur ce wiki, et pour cela nous t'invitons à consulter différentes pages qui t'aideront dans cette noble entreprise:

- [La structure du wiki](/wiki/structure)
- [La documentation du wiki](wiki/home)
- [Les règles d'édition du wiki](wiki/guidelines)

![Betatesting](/uploads/betatesting.jpg "Betatesting"){.align-center}