<!-- TITLE: Livre blanc -->
<!-- SUBTITLE: Document présentant les idées conductrices du projet Territoires en Commun -->

# Présentation
> A COMPLETER : Présentation du concept de livre blanc + objectifs / Mentionner les versions précédentes - accessibles en lien en fin de page


La dernière version du livre blanc est accessible [ici](https://drive.google.com/file/d/13az31LHIzvp1Yb_ZYs58TZSGqSgbPEVO/view?usp=sharing).

# Résumé
# 1/ Un monde en transition
# 2/ Le Pays du Bocage
# 3/ Quel modèle de société pour demain?
# 4/ Comment co-construire nos territoires?
# 5/ Notre proposition
# 6/ Les thématiques de travail
# 7/ La feuille de route
# 8/ Cartographie des projets du territoire
# 9/ Quels projets pour demain?
# 10/ Qui sommes-nous?
# 11/ Comment s'impliquer?
# Annexes
## Annexe 1 - Historique du projet
## Annexe 2 - Liste des personnes rencontrées
## Annexe 3 - Les fiches thématiques 
### Fiche thématique 1 - L'agriculture et les circuits courts
### Fiche thématique 2 - L'énergie
### Fiche thématique 3 - L'économie circulaire et locale
### Fiche thématique 4 - La santé et les services à la personne
### Fiche thématique 5 - L'attractivité du territoire
