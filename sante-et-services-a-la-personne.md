<!-- TITLE: Santé et services à la personne -->


# 1/ Les enjeux
ff

# 2/ Le contexte local
ff

# 3/ Les acteurs du territoire
ff

# 4/ Les pistes d'action
dd

# 5/ Références
dd

# 6/ Pages connexes
* [Habitat intergénérationnel](sante-et-services-a-la-personne/habitat-intergenerationnel)