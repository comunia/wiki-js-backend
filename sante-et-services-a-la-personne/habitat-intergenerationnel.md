<!-- TITLE: Habitat Intergénérationnel -->


# Présentation

Le concept de Logement intergénérationnel ou Cohabitation intergénérationnelle existe depuis 2004 en France. Il a commencé à être défini juridiquement dans le cadre de la Loi d'Adaptation de la Société au Vieillissement (Article 17). Il diffère de celui d'habitat intergénérationnel, défini par le Ministère de solidarités et de la santé comme "un ensemble de logements conçu pour accueillir différentes générations : étudiants, familles, personnes âgées".

Il s'agit pour un senior de mettre à disposition d'un jeune une chambre. Le dispositif est soit gratuit pour le jeune, soit une indemnité d'occupation significativement en deçà des prix pratiqués sur le marché locatif est versée par le jeune au senior. Les objectifs du logement ou de la cohabitation intergénérationnel sont de :

* Faciliter l’expression de la solidarité, de l’engagement et rapprocher les générations en France métropolitaine et DOM TOM
* Étendre l’offre de logements et développer ainsi la mobilité des jeunes
* Lutter contre l’isolement des seniors, des jeunes, et rassurer les familles de ceux-ci
* Prévenir la perte d’autonomie des seniors
* Optimiser la consommation énergétique par l’utilisation des chambres laissées vacantes

[video](https://www.youtube.com/embed/RqrKpaTyUaU){.youtube} {.align-center}
# Enjeux
## Enjeux directs
> A COMPLETER
## Enjeux croisés
> A COMPLETER
# Projets d'intérêt
> A COMPLETER

# Les groupes de réflexion
* [L'habitat intergénérationnel dans le Pays du Bocage](/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel)

# A voir aussi
## Pages connexes

## Liens externes

* [La résidence intergénérationnelle : une piste plus prometteuse que la cohabitation ?](https://www.banquedesterritoires.fr/la-residence-intergenerationnelle-une-piste-plus-prometteuse-que-la-cohabitation?cid=1250268367434&pagename=Territoires/LOCActu/ArticleActualite), article de la Banque des Territoires (29 janvier 2015).
* [Habitat , territoires et vieillissement: un nouvel apprentissage](https://www.cairn.info/revue-gerontologie-et-societe1-2011-1-page-29.htm), article publié dans gérontologie et société (2011).
* [L'adaptation de l'habitat à l'évolution démographique : Un chantier d'avenir](https://www.ladocumentationfrancaise.fr/docfra/rapport_telechargement/var/storage/rapports-publics/094000489.pdf), rapport remis au Secrétaire d'Etat au logement et à l'urbanisme (octobre 2009).
* [Le logement intergénérationnel : évaluation de l’offre et de la demande potentielle](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwjpoorvhOHeAhWEhKYKHRtLDAwQFjAAegQICRAC&url=https%3A%2F%2Fwww.caf.fr%2Fsites%2Fdefault%2Ffiles%2Fcnaf%2FDocuments%2FDser%2Fdossier_etudes%2Fdossier_132_-_le_logement_intergenerationnel.pdf&usg=AOvVaw2sFfsCia1V_U0O0Y_qLD6k), dossier d'études commandité par la CNAF (septembre 2010).

