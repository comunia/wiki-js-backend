<!-- TITLE: Création de compte -->

# Créer un compte
Deux méthodes de création de compte sont possibles, elles sont détaillées ci-après. 

## En utilisant un compte social (Google ou Facebook)

Pour ceux qui disposent d'un compte Facebook ou d'un compte Google, il suffit de se rendre sur la page de connection en cliquant sur le bouton [S'identifier](/login) dans le menu à gauche, puis de cliquer sur le bouton `Facebook` ou `Google ID`.  Après avoir donné ton accord pour le partage de ton adresse mail et de ton nom, ton compte sera automatiquement créé.

## En utilisant une adresse email

Si la méthode précédente ne te convient pas, nous pouvons te créer un compte à partir de ton adresse email. Il te suffit alors de nous envoyer un mail à wiki@territoires.co avec l'objet "Création de compte". Nous te créerons un compte et te transmettrons un mot de passe provisoire que tu pourras modifier en allant (une fois connecté) dans "Paramètres" puis dans "Mon Profil".
# Obtenir les droits d'édition
Pour protéger ce wiki des robots et des attaques, les droits d'édition sont gérés manuellement par notre équipe. Si tu souhaites contribuer, il te suffit de nous contacter en écrivant à wiki@territoires.co (la demande de droits peut se faire en même temps que la demande de création de compte). Tu peux alors nous préciser:

- Les thématiques sur lesquelles tu souhaiterais contribuer (ex: l'agriculture, l'habitat intergénérationnel) ;
- Qui tu es, tes projets, comment tu as entendu parler de Territoires en Commun, et autres informations qui te sembleront pertinentes...

