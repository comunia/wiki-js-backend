<!-- TITLE: Documentation du Wiki -->
<!-- SUBTITLE: On va vous aider à devenir un pro du wiki -->

# Les premiers pas
## La création de compte

Le procédure de création de compte est détaillée sur [cette page](/wiki/creation-de-compte). Un compte te permettra d'ajouter de nouvelles pages, de modifier du contenu existant et d'enrichir le wiki avec l'ajout de documents, de liens, d'images et de vidéos.

## La navigation

Cette plateforme wiki est organisée autour d'une structure que nous présentons plus en détails sur [cette page](/wiki/structure). Tu peux naviguer sur le wiki de trois principales façons:

1. **En utilisant le moteur de recherche (en haut de page):** tu peux y rentrer un mot clef (ex: énergie) et choisir une des pages proposées par le moteur (ex: Scénario négaWatt) ;
2. **En partant d'un des portails thématiques listés sur la page d'accueil:** la liste de nos portails thématiques est présentée sur la page d'accueil. Tu pourras alors naviguer d'une page à une autre au fil de ta lecture ;
3. **En partant d'un portail géographique:** la liste des territoires participant à ce projet est listé sur [cette page](https://wiki.territoires.co/france/normandie/orne/pays-du-bocage/projet-de-recherche-collaborative). Tu pourras alors voir les travaux, réflexions et projets menés au sein de chaque territoire participant à la démarche.

# Comment contribuer au wiki?
## En ajoutant de nouvelles pages

Pour ajouter une nouvelle page, il te faut tout d'abord être connecté en tant qu'utilisateur. Si ce n'est pas le cas, tu peux cliquer sur "S'identifier" dans le menu vertical de gauche. Tu seras alors redirigé vers la page d'accueil.

Une fois connecté.e, il te faut déterminer : 

**1. Quels sont tes droits d'édition?** Afin de sécuriser la plateforme, notre équipe donne des droits d'édition limités à certaines parties du wiki. Le plus souvent, tu disposeras de droits te permettant d'ajouter du contenu relatif à ton territoire de résidence (ex: Le Pays du Bocage) ou d'une thématique d'intérêt (ex: l'habitat intergénérationnel).
**2. Quel type de contenu souhaites-tu créer?** Dans la plupart des cas, tu souhaiteras créer l'une des pages suivantes: un portail géographique, un portail thématique, une page thématique ou bien une fiche documentaire. 
3. **Dans quelle partie du wiki souhaites-tu intégrer cette nouvelle page?** Tu dois à présent rechercher la page du wiki qui te semble la plus pertinente pour y rattacher ce nouveau contenu. S'il s'agit d'une fiche documentaire présentant un diagnostic agricole réalisé sur le territoire du Pays du Bocage, une page d'attache pertinente serait le portail thématique "[Agriculture et circuits courts - Pays du Bocage](/france/normandie/orne/pays-du-bocage/agriculture-et-circuits-courts)".

Une fois que tu as répondu à ces quelques questions, il te suffit de te rendre sur la page d'attache et de cliquer sur "Créer" (en haut à droite de la page). Il te sera alors demandé de renseigner un chemin (un nom pour cette page) et de cliquer sur "Créer". Sur la nouvelle page créée, tu peux commencer par modifier le titre et le sous-titre puis ajouter de nouvelles sections.  L'édition d'une page est expliquée dans la section suivante.  

## En éditant du contenu existant

L'ajout ou la modification de contenu se fait en cliquant sur "Editer" (dans le menu horizontal, en haut à droite). 

Une fois sur la partie modifiable de la page, tu peux diviser une page en plusieurs parties en cliquant sur H1 pour ajouter un titre de paragraphe (ou en ajoutant un # et un espace devant le titre), sur H2 pour un sous-titre (en ajoutant deux #) et sur H3 pour un sous-sous-titre (en ajoutant 3 #).

La barre d'édition horizontale propose d'autres fonctionnalités telles que le passage du texte en gras et/ou italique, l'ajout de listes (en bullet points ou numérotées) et l'ajout d'éléments externes (fonctionnalité présentée dans la section suivante). 

## En intégrant des éléments externes (documents, images, vidéos)
### L'ajout de documents
>  A COMPLETER

### L'ajout d'images
>  A COMPLETER

### L'ajout de vidéos
>  A COMPLETER
# Pour aller plus loin
## Le Markdown (langage informatique du wiki)

Les pages de ce wiki sont écrites en utilisant un langage de formatage appelé [Markdown](https://fr.wikipedia.org/wiki/Markdown). Il s'agit d'un langage de balisage très simple permettant de produire des pages web sans avoir à connaître le langage *html*.

Voici quelques ressources utiles pour découvrir le Markdown ou approfondir vos connaissances:
- [Une antisèche Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) *(en anglais)*
- [Un éditeur Markdown en ligne](https://jbt.github.io/markdown-editor/) avec la prévisualisation du résultat en temps-réel. Très pratique pour expérimenter
- [Un tutoriel vidéo](https://www.youtube.com/watch?v=6A5EpqqDOdk) *(en anglais)*

## L'historique d'édition

Ce wiki utilise [Git](https://fr.wikipedia.org/wiki/Git) comme mécanisme de stockage et versionnage du contenu. Chaque modification du wiki est enregistré dans un *repository git* hebergé par [Gitlab.com](https://gitlab.com). Vous pouvez librement consulter:
- [Le repository git du wiki](https://gitlab.com/comunia/wiki-js-backend)
- [L'historique des révisions du wiki](https://gitlab.com/comunia/wiki-js-backend/commits/master)
- [Les statistiques d'éditions](https://gitlab.com/comunia/wiki-js-backend/graphs/master/charts)
- [Les contributeurs](https://gitlab.com/comunia/wiki-js-backend/graphs/master)