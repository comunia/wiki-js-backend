<!-- TITLE: Structure du wiki -->

# Les entrées géographiques

Le wiki ayant pour vocation de faciliter la collaboration entre acteurs d'un territoire délimité, nous proposons de structurer la plateforme autour d'entrées géographiques. Les échelons suivants ont notamment été définis:

* L'échelon national (ex : la France)
* L'échelon régional (ex : la Normandie)
* L'échelon départemental (ex: l'Orne)
* L'échelon territorial (ex: [le Pays du Bocage](/france/normandie/orne/pays-du-bocage)) 

# Les entrées thématiques
Les entrées géographiques peuvent ensuite être croisées avec des entrées thématiques. Nous proposons ici de distinguer trois niveaux allant du plus général au plus spécifique : les portails thématiques, les pages thématiques et les fiches documentaires.

## Les portails thématiques

Les portails rassemblent des informations en lien avec une problématique générale pouvant être de nature globale, nationale ou locale. Les portails suivants ont notamment été créés au lancement de cette plateforme:

* Portail Agriculture et circuits courts
* Portail Economie circulaire et locale
* Portail Transition numérique
* Portail Attractivité du territoire
* Portail Santé et services à la personne
* Portail Energie

Chacun de ces portails peut ensuite être décliné aux échelons géographiques pertinents (ex: Portail Agriculture et circuits courts - Pays du Bocage). L'objectif de ces différents portails thématiques est double : (i) organiser l'information collectée et créée par la communauté (rapports, données, analyses, cartographies, etc.) ; et (ii) référencer les pages thématiques alimentées par des groupes de réflexion (ex: [Groupe de réflexion sur l'habitat intergénérationnel dans le Pays du Bocage](/france/normandie/orne/pays-du-bocage/groupes-de-reflexion/habitat-intergenerationnel)).

## Les pages thématiques

Les pages thématiques rassemblent des informations plus orientées sur le montage de projet et les retours d'expérience, l'objectif étant de passer de l'analyse d'une problématique générale (réalisée sur les portails thématiques) à l'étude de solutions concrètes (réalisée sur les pages thématiques). Les pages suivantes ont par exemple été créées:

* Page sur l'habitat intergénérationnel (rattachée au portail Santé et services à la personne)
* Page sur la méthanisation (rattachée au portail Energie)

## Les fiches documentaires

Les fiches documentaires présentent des éléments d'information plus spécifiques tels que des rapports, des études, des analyses, des jeux de données ou des présentations de projet. Ces fiches peuvent être rattachées à un portail thématique, une page thématique ou encore à une autre fiche documentaire. Les fiches suivantes donnent une illustration pour chacun de ces cas de figure:

* [Fiche sur le Programme Régional de Développement Agricole et Rural](/agriculture-et-circuits-courts/programme-regional-de-developpement-agricole-et-rural) (rattachée au portail Agriculture et circuits courts - Normandie)
* Fiche de Revue de littérature sur l'habitat intergénérationnel (rattachée à [la page thématique Habitat intergénérationnel](/sante-et-services-a-la-personne/habitat-intergenerationnel))
* [Fiche de présentation du Scénario négaWatt](/energie/scenario-nega-watt-2017) (rattachée à [la fiche de présentation du Scénario Afterres2050](/agriculture-et-circuits-courts/scenario-afterres-2050))